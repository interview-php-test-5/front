import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

import router from './router.js'
import store from './store'

import './assets/css/style.scss'

Vue.use(VueRouter)
Vue.use(VueResource)

import apiConfig from './config/apiConfig'

Vue.http.options.root = apiConfig.url

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
