import VueRouter from 'vue-router'

import EmployeeList from './views/EmployeeList'
import HomePage from './views/HomePage'
import NotFoundPage from './views/NotFoundPage'

export default new VueRouter({
	base: '/',
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'home',
			component: HomePage
		},
		{
			path: '/employees',
			name: 'employees',
			component: EmployeeList,
		},
		{
			path: '/employees/:department_id',
			name: 'department-employees',
			component: EmployeeList,
		},
		{
			path: '*',
			component: NotFoundPage
		}
	]
})
