import Vue from 'vue'
import Vuex from 'vuex'

import EmployeeStore from '@/stores/EmployeeStore'

Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
		EmployeeStore
	}
})

export default store
