import Vue from 'vue'

export default {
	namespaced: true,
	state: {
		list: [],
		meta: {
			page: 1,
			perPage: 10
		},
		departments: []
	},
	actions: {
		async getEmployees({ commit }, query) {
			commit('setList', { meta: null, data: [] })
			let response = await Vue.http.get('employee', { params: query })
			commit('setList', response.body)
		},
		async getDepartments({ commit }) {
			let response = await Vue.http.get('department')
			commit('set', { type: 'departments', value: response.body })
		}
	},
	mutations: {
		set(state, { type, value }) {
			state[type] = value
		},
		setList(state, response) {
			state.list = response.data
			state.meta = {
				total: response.total,
				numberOfPages: response.last_page,
				from: response.from,
				to: response.to,
				page: response.current_page
			}
		}
	}
}
